# Santex RBI Team - Front End Training Challenge

Quick challenge to help candidates to join RBI Team to catch up with currently used technologies

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Goals

- Get familiar with Styled Components as styling strategy
- Get a good understanding of Apollo Client and how to integrate Graphql to a React front end application
- Use Graphql Fragments
- Acquire good practices with Jest and testing both components and hooks
- Review React hooks concepts and develop custom hooks

## Requirements

- Implement a home page with a grid of products that includes product picture, description and price (from any product variant). Hint: use Graphql query.
- Implement app header component that includes the subtotal of the current order and persists through page refresh. Hint: use Graphql mutation and Context API
- Add custom hook named `useStateWithStorage` with same API as `useState` hook but adding local storage capabilities. Can be used for header subtotal
- Create tests for grid UI item and other components

## API documentation

Even thought the app is already connected to a graphql endpoint, the candidate can find here all required information about `queries`, `mutations` and Graphql types.

- https://www.vendure.io/docs/graphql-api/shop/

## Notes

This is what I believe can be improved and I didn't prioritize:

- Add a `Loader` or `Spinner` component instead of showing a text for when the product list is still loading.
- Include unit tests for when the mutation `addItemToOrder` retrieves typename InsufficientStockError, OrderModificationError or OrderLimitError. I implemented NegativeQuantityError since the rest are quite similar and this unit test serves as a good example.
- Move services outside `Product.tsx` and `ProductList.tsx`.
- Handle different colors for successful and error messages (e.g. green and red scales).
- Add button to add/remove quantities before adding the selected product to the order and also an input field to enter the desired quantity.

## Scripts

### `yarn`

Installs all dependencies.

### `yarn start`

Runs the app in the development mode.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
