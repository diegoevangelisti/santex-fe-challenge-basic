import { MockedProvider } from "@apollo/client/testing";
import {
  render,
  waitFor,
  screen,
  fireEvent,
  act,
} from "@testing-library/react";
import { AddItemToOrder } from "../../graphql/mutations";
import { Product } from "./Product";
import { imgSource, product } from "../ProductList/ProductList.test";

describe("Product", () => {
  it("shows product description, image and price and a product can be added to the order", async () => {
    const productMock = {
      request: {
        query: AddItemToOrder,
        variables: {
          productVariantId: "test-id",
          quantity: 1,
        },
      },
      result: {
        data: {
          addItemToOrder: {
            __typename: "Order",
            subTotal: product.variants[0].price,
          },
        },
      },
    };
    render(
      <MockedProvider mocks={[productMock]} addTypename={false}>
        <Product product={product} />
      </MockedProvider>
    );

    await act(() => new Promise((resolve) => setTimeout(resolve, 0)));
    const { getByText, getByTestId } = screen;

    const productImage = getByTestId("product-image");

    await waitFor(() => {
      expect(getByText("test-description")).toBeInTheDocument();
      expect(getByText(/1000/)).toBeInTheDocument();
      expect(productImage).toBeInTheDocument();
      expect(productImage).toHaveAttribute("src", imgSource);
      expect(productImage).toHaveAttribute("width", "100%");
      expect(productImage).toHaveAttribute("height", "auto");
    });

    fireEvent.click(getByTestId("product-button"));

    await act(() => new Promise((resolve) => setTimeout(resolve, 0)));

    waitFor(() =>
      expect(
        getByText("Product successfully added to the order!")
      ).toBeInTheDocument()
    );
  });

  it("shows an error message on failed addItemToOrder mutation", async () => {
    const productMock = {
      request: {
        query: AddItemToOrder,
        variables: {
          productVariantId: "test-id",
          quantity: 1,
        },
      },
      error: new Error("error"),
    };
    render(
      <MockedProvider mocks={[productMock]} addTypename={false}>
        <Product product={product} />
      </MockedProvider>
    );

    const { getByTestId, getByText } = screen;
    fireEvent.click(getByTestId("product-button")); // fires the mutation

    await new Promise((resolve) => setTimeout(resolve, 0));

    waitFor(() =>
      expect(getByText("Error! Please try again")).toBeInTheDocument()
    );
  });

  it("shows an error message when trying to add a negative quantity", async () => {
    const productMock = {
      request: {
        query: AddItemToOrder,
        variables: {
          productVariantId: "test-id",
          quantity: -1,
        },
        result: {
          data: {
            addItemToOrder: {
              message: "error-message",
            },
          },
        },
      },
    };
    render(
      <MockedProvider mocks={[productMock]} addTypename={false}>
        <Product product={product} />
      </MockedProvider>
    );

    await new Promise((resolve) => setTimeout(resolve, 0));

    const { getByText } = screen;
    waitFor(() => expect(getByText("error-message")).toBeInTheDocument());
  });
});
