export enum OrderType {
  Order = "Order",
}
export type Order = {
  subTotal: number;
  __typename: OrderType.Order;
};

export type Error = {
  message: string;
  __typename:
    | "NegativeQuantityError"
    | "InsufficientStockError"
    | "OrderModificationError"
    | "OrderLimitError";
};

export type AddItemToOderResult = Order | Error;
