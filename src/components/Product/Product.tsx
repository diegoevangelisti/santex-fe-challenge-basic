import { useMutation } from "@apollo/client";
import { useEffect, useState } from "react";
import { AddItemToOrder } from "../../graphql/mutations";
import { useStateWithStorage } from "../../hooks/useStateWithStorage";
import { Col, Text, Box, Image, Card, Button } from "../styles";
import { ProductType } from "../types";
import { Error, Order, OrderType } from "./types";

export const Product = ({ product }: { product: ProductType }) => {
  const {
    description,
    variants,
    featuredAsset: { source },
  } = product;

  const [message, setMessage] = useState("");
  const { subTotal, setSubTotal } = useStateWithStorage();

  const handleMessage = (result: Order | Error) => {
    let message = "";
    switch (result.__typename) {
      case OrderType.Order:
        message = "Product successfully added to the order!";
        break;
      default:
        // Handle message for all error types here
        message = "Error!" + result.message;
        break;
    }
    setMessage(message);
  };

  const [addItemToOrder] = useMutation(AddItemToOrder, {
    onCompleted: (data) => {
      if (data.addItemToOrder.__typename === OrderType.Order) {
        // Add recently added product price to the previous subTotal
        setSubTotal(subTotal + data.addItemToOrder.subTotal);
      }
      handleMessage(data.addItemToOrder);
    },
    onError: () => {
      // Show an error message in case the mutation fails
      setMessage("Error! Please try again");
    },
  });

  // Hide the message and show the button again after 3 seconds
  useEffect(() => {
    if (message !== "") {
      setTimeout(() => {
        setMessage("");
      }, 3000);
    }
  }, [message]);

  return (
    <Card>
      <Col size={1}>
        <Box>
          <Image
            data-testid="product-image"
            src={source}
            width="100%"
            height="auto"
            maxWidth="300px"
          />
        </Box>
      </Col>
      <Col size={3}>
        <Box>
          <Text center fontSize="30px">
            ${variants[0].price}
          </Text>
        </Box>
        <Box>
          <Text>{description}</Text>
        </Box>
        <Box center>
          {message !== "" ? (
            <Text bold fontSize="16px">
              {message}
            </Text>
          ) : (
            <Box>
              <Button
                data-testid="product-button"
                onClick={() =>
                  addItemToOrder({
                    variables: {
                      productVariantId: variants[0].id,
                      quantity: 1,
                    },
                  })
                }
              >
                Add item to order
              </Button>
            </Box>
          )}
        </Box>
      </Col>
    </Card>
  );
};
