import { MockedProvider } from "@apollo/client/testing";
import { act, render, screen } from "@testing-library/react";
import { GetProductList } from "../../graphql/queries";
import { ProductList } from "./ProductList";

export const imgSource =
  "https://www.shutterstock.com/image-vector/thin-line-snap-finger-like-easy-1070110487";

export const product = {
  id: "test-id",
  description: "test-description",
  variants: [{ id: 1, price: 1000 }],
  featuredAsset: {
    source: imgSource,
  },
};

describe("ProductList", () => {
  it("shows an error message when the query fails", async () => {
    const productListMock = {
      request: {
        query: GetProductList,
      },
      error: new Error("error"),
    };
    render(
      <MockedProvider mocks={[productListMock]} addTypename={false}>
        <ProductList />
      </MockedProvider>
    );

    await act(() => new Promise((resolve) => setTimeout(resolve, 0)));

    const { getByText } = screen;
    expect(getByText("Error loading the product list!")).toBeInTheDocument();
  });
  it("shows the loading message", async () => {
    render(
      <MockedProvider mocks={[]} addTypename={false}>
        <ProductList />
      </MockedProvider>
    );

    const { getByText } = screen;
    expect(getByText("Loading...")).toBeInTheDocument();
  });
});
