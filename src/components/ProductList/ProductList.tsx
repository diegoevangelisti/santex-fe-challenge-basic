import { useQuery } from "@apollo/client";
import { GetProductList } from "../../graphql/queries";
import { Product } from "../Product";
import { Text } from "../styles";
import { ProductType } from "../types";

export const ProductList = () => {
  const { data, loading, error } = useQuery(GetProductList);

  // Show loading state
  if (loading)
    return (
      <Text center color="#FFFFFF" fontSize="20px">
        Loading...
      </Text>
    );

  // Show error message when the query retrieves an error
  if (error)
    return (
      <Text center color="#FFFFFF" fontSize="20px">
        Error loading the product list!
      </Text>
    );

  return (
    <>
      {data.products.items.map((item: ProductType) => (
        <Product key={item.id} product={item} />
      ))}
    </>
  );
};
