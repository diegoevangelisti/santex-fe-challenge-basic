export type ProductType = {
  id: string | number;
  description: string;
  variants: VariantType[];
  featuredAsset: AssetType;
};

export type VariantType = {
  id: number;
  price: number;
};

export type AssetType = {
  source: string;
};

export type ColProps = {
  size: number;
  collapse?: string;
};

export type BoxProps = {
  center?: boolean;
};

export type TextProps = {
  center?: boolean;
  bold?: boolean;
  fontSize?: string;
  m?: string;
  color?: string;
};

export type ImageProps = {
  maxWidth?: string;
};

export type CardProps = {
  dark?: boolean;
};
