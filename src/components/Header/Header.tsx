import { useStateWithStorage } from "../../hooks/useStateWithStorage";
import { Box, Card, Text } from "../styles";

export const Header = () => {
  const { subTotal } = useStateWithStorage();

  return (
    <header>
      <Card dark>
        <Box>
          <Text bold fontSize="30px" color="#FFFFFF">
            Sub total
          </Text>
        </Box>
        <Box>
          <Text data-testid="subtotal-price" fontSize="30px" color="#FFFFFF">
            $ {subTotal}
          </Text>
        </Box>
      </Card>
    </header>
  );
};
