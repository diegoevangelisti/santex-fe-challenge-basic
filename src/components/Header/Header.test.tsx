import { render, waitFor, screen } from "@testing-library/react";
import { Header } from "./Header";

describe("Header", () => {
  it("shows the header text and initial value", async () => {
    render(<Header />);

    const { getByText } = screen;

    await waitFor(() => {
      expect(getByText("Sub total")).toBeInTheDocument();
      expect(getByText(/0/)).toBeInTheDocument();
    });
  });
});
