import styled, { css } from "styled-components";
import { BoxProps, CardProps, ColProps, ImageProps, TextProps } from "./types";

export const Row = styled.div`
  display: flex;
  align-items: center;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export const Card = styled(Row)<CardProps>`
  margin: 40px 20%;
  padding: 10px;
  border-radius: 5px;
  background: ${(props) => (props.dark ? "#000000" : "#FFFFFF")};
`;

export const Col = styled.div<ColProps>`
  flex: ${(props) => props.size};
`;

export const Box = styled.div<BoxProps>`
  margin: 10px;
  ${(props) => {
    if (props.center) {
      return css`
        display: flex;
        justify-content: center;
        align-items: center;
      `;
    }
  }}
`;

export const Text = styled.p<TextProps>`
  font-size: ${(props) => props.fontSize ?? "inherit"};
  text-align: ${(props) => props.center && "center"};
  font-weight: ${(props) => props.bold && "bold"};
  color: ${(props) => props.color ?? "inherit"};
`;

export const Image = styled.img<ImageProps>`
  max-width: ${(props) => props.maxWidth};
`;

export const Button = styled.button`
  width: 200px;
  height: 40px;
  font-size: 16px;
  background-color: #000000;
  color: #ffffff;
  border-color: #000000;
  &:active {
    background-color: #ffffff;
    color: #000000;
  }
  &:hover {
    cursor: pointer;
  }
`;
