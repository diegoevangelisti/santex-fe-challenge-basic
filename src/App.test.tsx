import { MockedProvider } from "@apollo/client/testing";
import { render, screen } from "@testing-library/react";
import { Header } from "./components/Header";
import { ProductList } from "./components/ProductList";
import { OrderProvider } from "./hooks/useStateWithStorage";

describe("App", () => {
  it("shows initial header and loading text", async () => {
    render(
      <MockedProvider mocks={[]} addTypename={false}>
        <OrderProvider>
          <Header />
          <ProductList />
        </OrderProvider>
      </MockedProvider>
    );

    const { getByText } = screen;
    expect(getByText("Sub total")).toBeInTheDocument();
    expect(getByText("Loading...")).toBeInTheDocument();
  });
});
