import { gql } from "@apollo/client";

export const GetProductList = gql`
  query products {
    products {
      items {
        id
        name
        description
        variants {
          id
          price
        }
        featuredAsset {
          source
        }
      }
    }
  }
`;
