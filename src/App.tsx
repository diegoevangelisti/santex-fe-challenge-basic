import { Header } from "./components/Header";
import { ProductList } from "./components/ProductList";
import { OrderProvider } from "./hooks/useStateWithStorage";

function App() {
  return (
    <OrderProvider>
      <Header />
      <ProductList />
    </OrderProvider>
  );
}

export default App;
