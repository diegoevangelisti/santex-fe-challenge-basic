import {
  createContext,
  useContext,
  useState,
  ReactNode,
  useEffect,
} from "react";

const SubTotalContext = createContext({
  subTotal: 0,
  setSubTotal: (_: number) => {},
});

export const OrderProvider = ({ children }: { children: ReactNode }) => {
  const [subTotal, setSubTotal] = useState(0);

  useEffect(() => {
    // Get initial value from the local storage or 0 if undefined
    const initialSubTotal = JSON.parse(localStorage.getItem("subTotal") || "0");
    setSubTotal(+initialSubTotal);
  }, []);

  useEffect(() => {
    // Set updated subTotal anytime the subTotal quantity changes
    localStorage.setItem("subTotal", JSON.stringify(subTotal));
  }, [subTotal]);

  return (
    <SubTotalContext.Provider value={{ subTotal, setSubTotal }}>
      {children}
    </SubTotalContext.Provider>
  );
};

export const useStateWithStorage = () => {
  const { subTotal, setSubTotal } = useContext(SubTotalContext);
  return { subTotal, setSubTotal };
};
